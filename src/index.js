import './css/style.css';
import rocketIcon from './images/rocket.svg';

function component() {
    const element = document.createElement('div');

    element.innerText = 'Hi from index';
    element.classList.add('hello');
    // Add the image to our existing div.
    const myIcon = new Image();
    myIcon.src = rocketIcon;

   element.appendChild(myIcon);

    return element;
  }

  document.body.appendChild(component());